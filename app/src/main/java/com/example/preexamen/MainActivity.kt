package com.example.preexamen

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var btnSalir: Button
    private lateinit var btnEntrar: Button
    private lateinit var txtNombre: EditText
    private lateinit var lblNombre: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarComponentes()
        btnEntrar.setOnClickListener { entrar() }
        btnSalir.setOnClickListener { salir() }
    }
    private fun iniciarComponentes(){
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
        txtNombre = findViewById(R.id.txtNombre)
    }
    @SuppressLint("SuspiciousIndentation")
    private fun entrar(){
        if ((txtNombre.text.toString().isEmpty())){
            Toast.makeText(this.applicationContext,"Nombre requerido",Toast.LENGTH_SHORT).show()
            return
        }
      val strNombre = txtNombre.text.toString()
        val intent = Intent(this,ReciboNominaActivity::class.java)
        intent.putExtra("strNombre",strNombre)
        startActivity(intent)

    }
    private fun salir(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Nomina")
        confirmar.setMessage("¿Deseas salir?")
        confirmar.setPositiveButton("Confirmar"){_,_->finish()}
        confirmar.setNegativeButton("Cancelar"){_,_->}.show()
    }
}